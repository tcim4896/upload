const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 3000;
const mongoose = require('mongoose');
const formidable = require('formidable');
const Schema = mongoose.Schema;
const http = require('http');
const bodyParser = require('body-parser')
const app = express();

app.use(bodyParser.json());
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.header('Access-Control-Allow-Credentials', true);
    next();
});

app.post('/', function (req, res){
    var form = new formidable.IncomingForm();

    form.parse(req);

    form.on('fileBegin', function (name, file){
        file.path = __dirname + '/uploads/' + file.name;
    });

    form.on('file', function (name, file){
        console.log('Uploaded ' + file.name);
    });

    res.sendFile(path.join(__dirname, 'dist')+ "/index.html");
});

app.use('/', express.static(path.join(__dirname, 'dist')))
 
app.all('*', (req, res) => { 
    res.status(200).sendFile(path.join(__dirname, 'dist')+ "/index.html");
});

app.listen(PORT, () => console.log(`Listening on ${ PORT }`));
